// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/AngularFP/slim/',
  firebase:{
    apiKey: "AIzaSyDvopHDBwoAcwIy07-k4ttYaRLP-3HNVdg",
    authDomain: "angularfp.firebaseapp.com",
    databaseURL: "https://angularfp.firebaseio.com",
    projectId: "angularfp",
    storageBucket: "angularfp.appspot.com",
    messagingSenderId: "318216802578"
  }
};
