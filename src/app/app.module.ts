import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';


import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule} from "angularfire2/database";
import {environment} from './../environments/environment';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ProfileComponent } from './profile/profile.component';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    NotFoundComponent,
    NavigationComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([ // בניה של הראוטס
      {pathMatch:'full',path:'users', component:UsersComponent},
      {pathMatch:'full',path: 'users/:id', component: UsersComponent},
      {pathMatch:'full',path: 'profile/:id', component: ProfileComponent},
      
      
      
     // {path:'message/:id', component:MessageComponent},
      {path:'**',component:NotFoundComponent}// צריך להופיע אחרון כי הוא תופס את כל מה שלא מוגדר ומעביר אליו

    ])
  
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
