import { Injectable } from '@angular/core';
import{Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import { environment } from '../../environments/environment';
import {AngularFireDatabase} from 'angularfire2/database';

@Injectable()
export class UsersService {
  db: AngularFireDatabase;
  http:Http;//http -> שם התכונה. Http-> סוג התכונה
  getUsers(){

    return this.http.get(environment.url+"users");


  }

  getUser(id){
    return this.http.get(environment.url+'user/' + id);
  }

  postUser(data) // דטה= נתונים של הטופס
  {
    let options = { // הגדרנו דרך המחלקה של אנגולר האדר , רכיב קונטנט טייפ בישביל שהנתונים יעברו
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
   let  params = new HttpParams().append('name', data.name).append('email',data.email);
   console.log(data) // פאראמס הוא מבנה נתונים המחזיק מפתח וערך 
   return this.http.post(environment.url+'users', params.toString(),options);

  }

    putUser(data,key){
        let options = {
          headers: new Headers({
            'content-type':'application/x-www-form-urlencoded'
          })
   
        }
        let params = new HttpParams().append('username',data.name).append('email',data.email);
        return this.http.put(environment.url+'user/'+ key,params.toString(), options);
      }

  deleteUser(key){
    return this.http.delete(environment.url+'user/'+key);
  }

  createPost(){
    return this.db.list('/posts').valueChanges();
  
  }

  getPosts(userId){
    return this.db.list('/posts').valueChanges();
  }

  getAllPosts(){

  }
  constructor(http:Http, db:AngularFireDatabase) {
    this.http = http;
    this.db =db;
   }

   

}
