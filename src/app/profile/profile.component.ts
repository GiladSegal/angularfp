import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'; // בכדי לתפוס את האיידי מהראוטר
import { UsersService } from '../users/users.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
posts;
  constructor(private rout:ActivatedRoute, private service:UsersService) { }

  ngOnInit() {
    this.rout.paramMap.subscribe(params=>{ //קריאת האיידי מהיואראל
      
            let id = params.get('id'); // שליפת האיידי
            console.log(id);
    this.service.getPosts(id).subscribe(response=>
    {
      
      console.log(response);
      this.posts=response;
    })
  }


}
